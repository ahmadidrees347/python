from django.contrib import admin

from .models import Customer
from .models import Product
from .models import Category
from .models import Cart
from .models import Shipper
from .models import Order
from .models import Address
from .models import SaleSummary
from .models import Student


class StudentAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'language', 'grades', 'gender')
    list_filter = ('language', 'gender', 'grades')
    save_as = True
    save_on_top = True
    change_list_template = 'change_list_graph.html'

admin.site.register(Student, StudentAdmin)

@admin.register(SaleSummary)
class SaleSummaryAdmin(admin.ModelAdmin):
    change_list_template = 'admin/sale_summary_change_list.html'
    #date_hierarchy = 'created'

class ProductAdmin(admin.ModelAdmin):
    list_display = ('P_name','P_quantity','P_price','P_image','Cat_id')
    list_display_links = ('P_name','P_image')
    list_filter = ('P_name', 'P_price','Cat_id')
    search_fields = ('P_name','P_price')
    save_as = True
    save_on_top = True
    change_list_template = 'change_list_graphProduct.html'
    fieldsets = (
        (None, {
            'fields': (
            'P_name',
            'P_quantity',
            'P_price',
            'P_image'
        )
        }),
    )

admin.site.register(Product, ProductAdmin)

class CartAdmin(admin.ModelAdmin):
    list_display = ('Cart_id','Cart_quantity','Cust_id','P_id')
    list_filter = ('P_id', 'Cust_id','Cart_id')
    save_as = True
    save_on_top = True
    change_list_template = 'change_list_graphCart.html'
    

admin.site.register(Cart, CartAdmin)

# Register your models here.
"""
class ShipperAdmin(admin.ModelAdmin):
    fieldsets = (
            (None, {
                    'fields': ('Shipper_phoneNo', )
            }),)
admin.site.register(Shipper, ShipperAdmin)
"""

class ShipperListFilter(admin.SimpleListFilter):
    title = 'Shipper'
    parameter_name = 'Shipper'

    default_value = None

    def lookups(self, request, model_admin):
        list_of_shipper = []
        queryset = Shipper.objects.all()
        for shipper in queryset:
            list_of_shipper.append(
                (str(shipper.Shipper_id), shipper.Shipper_CompanyName)
            )
        return sorted(list_of_shipper, key=lambda tp: tp[1])

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(Shipper_id=self.value())
        return queryset

    def value(self):
        value = super(ShipperListFilter, self).value()
        if value is None:
            if self.default_value is None:
                # If there is at least one Species, return the first by name. Otherwise, None.
                first = Shipper.objects.order_by('Shipper_CompanyName').first()
                value = None if first is None else first.Shipper_id
                self.default_value = value
            else:
                value = self.default_value
        return str(value)

#@admin.register(Shipper)
class ShipperAdmin(admin.ModelAdmin):
    """#fields = ['Shipper_phoneNo','Shipper_CompanyName']
    fieldsets = [
        (None,               {'fields': ['Shipper_phoneNo']}),
        ('Company Information', {'fields': ['Shipper_CompanyName']}),
    ]"""
    list_display = ('Shipper_id', 'Shipper_phoneNo', 'Shipper_CompanyName', )
    list_filter = (ShipperListFilter, )

admin.site.register(Shipper, ShipperAdmin)


admin.site.register(Customer)

admin.site.register(Category)

#admin.site.register(Shipper)
admin.site.register(Order)
admin.site.register(Address)