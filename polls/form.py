from django import forms
from .models import Category
from django.forms import ModelForm

class CategoryForm(forms.ModelForm):
    class Meta:
        fields = [
            'Cat_id',
            'Cat_type',
            'Cat_description'
        ]