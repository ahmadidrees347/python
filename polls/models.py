from django.db import models
from django.utils.text import slugify
from django.shortcuts import reverse


MALE, FEMALE = range(2)
GENDER = (
    (MALE, 'MALE'),
    (FEMALE, 'FEMALE')
)

CHINESE, SPANISH, ENGLISH, FRENCH, HINDI, ARABIC, RUSSIAN = range(7)
LANGUAGES = (
    (CHINESE, 'CHINESE'),
    (SPANISH, 'SPANISH'),
    (ENGLISH, 'ENGLISH'),
    (FRENCH, 'FRENCH'),
    (HINDI, 'HINDI'),
    (ARABIC, 'ARABIC'),
    (RUSSIAN, 'RUSSIAN'),
)


class Student(models.Model):
    full_name = models.CharField('Full Name', max_length=50)
    gender = models.PositiveSmallIntegerField('Gender', choices=GENDER, default=MALE)
    language = models.PositiveSmallIntegerField('Language', choices=LANGUAGES, default=ENGLISH)
    grades = models.CharField('Grades', max_length=2)

    def __str__(self):
        return self.full_name

    class Meta:
        verbose_name = ('Student')
# Create your models here.
class Snippet(models.Model):
    title =models.CharField(max_length=15)
    slug = models.SlugField(blank=True, null=True)
    body = models.TextField()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)
    def get_absolute_url(self):
        return  self.slug

class Customer(models.Model):
    Cust_id = models.AutoField(primary_key=True)
    Cust_name = models.CharField(max_length=255)
    Cust_email = models.CharField(unique=True, max_length=255)
    Cust_password = models.CharField(max_length=255)
    Cust_country = models.CharField(max_length=255)
    Cust_address = models.TextField()
    Cust_phoneNo = models.CharField(max_length=255)
    Cust_postalCode = models.IntegerField()
    def __str__(self):
        return self.Cust_name
    

class Category(models.Model):
    Cat_id = models.AutoField(primary_key=True)
    Cat_type = models.CharField(unique=True, max_length=255)
    Cat_description = models.TextField()
    def __unicode__(self):
        return self.Cat_type

class Product(models.Model):
    P_id = models.AutoField(primary_key=True)
    P_name = models.CharField(max_length=255)
    P_quantity = models.IntegerField()
    P_price = models.IntegerField()
    P_image = models.ImageField()
    Cat_id = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __int__(self):
        return self.P_id
    def __str__(self):
        return self.P_name

    class Meta:
        verbose_name = ('Product')

class Cart(models.Model):
    Cart_id = models.AutoField(primary_key=True)
    Cart_quantity = models.IntegerField()
    Cust_id = models.ForeignKey(Customer, on_delete=models.CASCADE)
    P_id = models.ForeignKey(Product, on_delete=models.CASCADE)

class Shipper(models.Model):
    Shipper_id = models.AutoField(primary_key=True)
    Shipper_phoneNo = models.CharField(max_length=25)
    Shipper_CompanyName = models.CharField(max_length=255)

class Order(models.Model):
    Order_id = models.AutoField(primary_key=True)
    Shipper_id = models.ForeignKey(Shipper, on_delete=models.CASCADE)
    Cust_id = models.ForeignKey(Customer, on_delete=models.CASCADE)
    P_id = models.ForeignKey(Product, on_delete=models.CASCADE)
class Address(models.Model):
    address_id = models.AutoField(primary_key=True)
    Cust_id = models.ForeignKey(Customer, on_delete=models.CASCADE)
    addressTitle = models.CharField(max_length=25)
    HouseNo = models.CharField(max_length=255)
    StreetNo = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
class SaleSummary(Cart):
    class Meta:
        proxy = True
        verbose_name = 'Sale Summary'
        verbose_name_plural = 'Sales Summary'