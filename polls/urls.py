from django.urls import path

from . import views

urlpatterns = [
    
    path('graph/', views.graph, name='graph'),
    path('', views.index, name='index'),
    path('mail/', views.mail, name='mail'),
    path('main/', views.mainPage, name='main'),
    path('AddAddress/', views.addAddress, name='AddAddress'),
    path('checkout/', views.checkout, name='checkout'),
    path('myAccount/', views.myAccount, name='myAccount'),
    path('categoryFemale/', views.categoryFemale, name='categoryFemale'),
    path('categoryMale/', views.categoryMale, name='categoryMale'),
    path('category/', views.category, name='category'),
    path('signIn/', views.signIn, name='signIn'),
    path('signUp/', views.signUp, name='signUp'),
    path('create/', views.create, name='create' ),
    path('authenticate/', views.authenticate, name='authenticate' ),
    path('signOut/', views.signOut, name='signOut' ),
    path('addCart/<int:id>/', views.addCart, name='addCart' ),
    path('deletecart/<id>/', views.deletecart, name='deletecart' ),
    path('invoice/', views.invoice, name='invoice' ),

    path('addCustomer/', views.addCustomer, name='addCustomer' ),
    path('manageCustomer/', views.manageCustomer, name='manageCustomer'), 
    path('update/<int:id>/', views.update, name='update' ),
    path('edit/<int:id>/', views.edit, name='edit' ),   
    path('addUser/', views.addUser, name='addUser' ),
    #form
    path('catForm/', views.categoryFormView, name='categoryFormView'),
    #SiteMap
    path('about/', views.about, name='about'),
    path('<slug:slug>/', views.snippet_detail),
    
]