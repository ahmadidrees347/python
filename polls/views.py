from django.shortcuts import render,redirect, get_object_or_404
from .models import Customer
from .models import Product
from .models import Cart, Snippet
from .form import CategoryForm
from django.http import HttpResponse
from django.contrib import messages
from django.core.paginator import Paginator
from django.core.mail import send_mail
from django.conf import settings
import array as arr


def categoryFormView(request):
   form =CategoryForm(request.POST or None)
   if form.is_valid():
      form.save()
      form = CategoryForm()
   context = {
      'form' : form 
   }
   return render(request, 'best/formDetail.html', context)

def graph(request):
    return render(request, 'best/sale_summary_change_list.html')
def index(request):
    return render(request, 'best/dashboard.html')

def about(request):
   return HttpResponse("About Page....")

def snippet_detail(request, slug):
   snippet = get_object_or_404(Snippet, slug=slug)
   return HttpResponse('Detail View for slug of {{slug}}')

def create(request):
    print(request.POST)
    cust_name = request.GET['fullname']
    cust_email = request.GET['email']
    cust_password = request.GET['password']
    cust_country = request.GET['country']
    cust_address = request.GET['homeAddress']
    cust_phoneNo = request.GET['phoneNo']
    cust_postalCode = request.GET['postalCode']
    Insert_customer = Customer(Cust_name=cust_name, Cust_email=cust_email, Cust_password=cust_password,
     Cust_country=cust_country, Cust_address=cust_address,Cust_phoneNo=cust_phoneNo, Cust_postalCode=cust_postalCode)
    Insert_customer.save()
    messages.success(request, 'User Register Sucessful')
    return render(request,'best/dashboard.html')
    #return HttpResponse("Hello, world. You're at the polls index.!")

def authenticate(request):
    print(request.POST)
    cust_email = request.GET['email']
    cust_password = request.GET['password']
    customers = Customer.objects.filter(Cust_email=cust_email,Cust_password=cust_password).count()
    
    if customers>0:
        #name = Customer.objects.filter(Cust_email=cust_email).values('Cust_name')
        request.session['email'] = cust_email
        #return HttpResponse(name)
        #request.session['name'] = name.Cust_name
        return render(request,'best/dashboard.html')
    return render(request,'best/SignIn.html')

def signOut(request):
   try:
      del request.session['email']
   except:
      pass
   return render(request,'best/dashboard.html')

def deletecart(request, id):
    cart = Cart.objects.get(pk=id)
    cart.delete()
    return redirect('/checkout')

def addCart(request, id):
   #'loggedin': request.session.get('email')
   if request.session.get('email') != "":
      productInfo = Product.objects.get(pk=id)
      p_id = productInfo.P_id
      c_quantity = 1
      cust_id = Customer.objects.get(Cust_email=request.session.get('email'))
      insert_cart = Cart(Cart_quantity=c_quantity, Cust_id=cust_id, P_id=Product.objects.get(pk=id))
      insert_cart.save()
      return redirect('../../checkout')
      #return render(request,'best/checkout.html')
   else:
      return render(request,'best/SignIn.html')
   #cart = Cart(request)
   #product = get_object_or_404(Product, id=product_id)

def mail(request):
   subject = 'Hello'
   message = 'Hi'
   from_email = settings.EMAIL_HOST_USER
   tmail = [settings.EMAIL_HOST_USER, 'ahmadidrees347@gmail.com']
   send_mail(subject, message, from_email, tmail, fail_silently=True)
   return HttpResponse('Thanku')  

def mainPage(request):
    return render(request,'best/dashboard.html')
def addAddress(request):
    return render(request,'best/AddAddress.html')

def invoice(request):
     if request.POST.get('checkbox'):
        return HttpResponse("OK")
     else:
        #qnty = request.POST.get('qnty')
        #qnty = request.GET['qnty']
        #return HttpResponse("OK"+str(qnty))
        cust_id = Customer.objects.get(Cust_email=request.session.get('email'))
        customers = Customer.objects.filter(Cust_email=request.session.get('email'))
        carts = Cart.objects.filter(Cust_id=cust_id)
        products = Product.objects.all();
        Tprice = 0;
        i=0;
        myList = [];
        obj = Product.objects.all()
        obj.a = lambda: None


        a = arr.array('d', myList)
        for cart in carts:
         for product in products:
            if product.P_id == cart.P_id.P_id:
               total_price = product.P_price*cart.Cart_quantity;
               setattr(obj.a, 'price', total_price)
               product.price = product.P_price*cart.Cart_quantity

               myList.append(product.P_price*cart.Cart_quantity);
               Tprice += total_price;
               i=i+1
        context = {
           'carts': carts,
           'products':products,
           'customers':customers,
           'total_price':total_price,
           'Tprice':Tprice,
           'myList': myList
        }
        #return HttpResponse("OK"+str(context))
        return render(request,'best/invoice.html',context)

def checkout(request):
    cust_id = Customer.objects.get(Cust_email=request.session.get('email'))
    customers = Customer.objects.filter(Cust_email=request.session.get('email'))
    carts = Cart.objects.filter(Cust_id=cust_id)
    products = Product.objects.all();
    context = {
        'carts': carts,
        'products':products,
        'customers':customers
    }
    #return HttpResponse(carts)
    return render(request,'best/checkout.html',context)
def myAccount(request):
    return render(request,'best/My Account.html')
def category(request):
    posts = Product.objects.all()
    paginator = Paginator(posts, 3) # < 3 is the number of items on each page
    page = request.GET.get('page') # < Get the page number

    posts = paginator.get_page(page)
    products = Product.objects.all()
    context = {
        'products': products,
        'posts': posts
    }
    #return HttpResponse(products)
    return render(request,'best/category.html',context)
def categoryFemale(request):
    posts = Product.objects.filter(Cat_id=2)
    paginator = Paginator(posts, 3) # < 3 is the number of items on each page
    page = request.GET.get('page') # < Get the page number

    posts = paginator.get_page(page)
    products = Product.objects.all()
    context = {
        'products': products,
        'posts': posts
    }
    #return HttpResponse(products)
    return render(request,'best/category.html',context)
def categoryMale(request):
    posts = Product.objects.filter(Cat_id=1)
    paginator = Paginator(posts, 3) # < 3 is the number of items on each page
    page = request.GET.get('page') # < Get the page number

    posts = paginator.get_page(page)
    products = Product.objects.all()
    context = {
        'products': products,
        'posts': posts
    }
    #return HttpResponse(products)
    return render(request,'best/category.html',context)
def signIn(request):
    return render(request,'best/SignIn.html')
def signUp(request):
    return render(request,'best/SignUp.html')
def manageCustomer(request):
    customers = Customer.objects.all()
    context = {
        'customers': customers
    }
    return render(request,'best/ManageCustomers.html',context)

def addCustomer(request):
    return render(request,'best/addCustomer.html')

def addUser(request):
    print(request.POST)
    cust_name = request.POST["cname"]
    cust_email = request.POST.get('cemail')
    cust_password = request.POST.get('cpassword')
    cust_country = request.POST.get('ccountry')
    cust_address = request.POST.get('caddress')
    cust_phoneNo = request.POST.get('cphoneNo')
    cust_postalCode = request.POST.get('cpostalCode')
    Add_customer = Customer(Cust_name=cust_name, Cust_email=cust_email, Cust_password=cust_password,
     Cust_country=cust_country, Cust_address=cust_address,Cust_phoneNo=cust_phoneNo, Cust_postalCode=cust_postalCode)
    Add_customer.save()
    #return HttpResponse('OK')
    return redirect('/ManageCustomers.html')
def edit(request, id):
    customer = Customer.objects.get(pk=id)
    context = {
        'customer': customer
    }
    return render(request, 'best/editCustomer.html', context)
def update(request, id):
    print(request.POST)
    customers = Customer.objects.get(pk=id)
    customers.cust_name = request.POST['custname']
    customers.cust_email = request.POST['custemail']
    customers.cust_password = request.POST['custpassword']
    customers.cust_country = request.POST['custcountry']
    customers.cust_address = request.POST['custaddress']
    customers.cust_phoneNo = request.POST['custphoneNo']
    customers.cust_postalCode = request.POST['custpostalCode']
    customers.save()
    return redirect('/')
    